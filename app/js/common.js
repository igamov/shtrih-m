$(document).ready(function() {
$('.news-carousel').owlCarousel({
    loop:true,
    margin:15,
    nav:true,
    navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
})
$('.clients-carousel').owlCarousel({
    loop:true,
    margin:15,
    nav:true,
    navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
    autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:6
        },
        1000:{
            items:8
        }
    }
})
$('#slider').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    items: 1,
    autoplay:true,
    autoplayTimeout:3000,
    smartSpeed: 1200
})
$('.navi').click(function(){
    _this = $(this);
    if(_this.hasClass('active')){
        _this.removeClass('active');
    }else{
        _this.addClass('active');
    }
    $('#nav-xs').slideToggle();
})
});