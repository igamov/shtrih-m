<?php
ini_set("soap.wsdl_cache_enabled", "0");
if(!define('MODX_API_MODE')){
define('MODX_API_MODE', true);
require $_SERVER["DOCUMENT_ROOT"]. '/index.php';
}
// Pull in the NuSOAP code
require_once('lib/nusoap.php');
// Create the server instance
$server = new soap_server();
$server->decode_utf8 = false; 
$server->soap_defencoding = 'UTF-8';
// Initialize WSDL support
$server->configureWSDL('getOrdersWsdl', 'http://shtrih-m.igamov.ru/assets/soap','','document');
// Register the method to expose
$server->register('getOrders',     // название метода 
 array('login' => 'xsd:string', 'password' => 'xsd:string'),  // входные параметры 
 array('return' => 'xsd:string'), // выходные параметры
 'urn:getorderswsdl',     // пространство имен 
  '', // soapaction 
  '', // стиль 
  '', // использование 
  '' // описание 
);

$server->register('changeStatus',     // название метода 
 array('idOrder' => 'xsd:string', 'login' => 'xsd:string', 'password' => 'xsd:string'),  // входные параметры 
 array('return' => 'xsd:boolean'), // выходные параметры
 'urn:getorderswsdl',     // пространство имен 
  '', // soapaction 
  '', // стиль 
  '', // использование 
  '' // описание 
);

$server->register('authentication',     // название метода 
 array('login' => 'xsd:string', 'password' => 'xsd:string'),  // входные параметры 
 array('return' => 'xsd:boolean'), // выходные параметры
 'urn:getorderswsdl',     // пространство имен 
  '', // soapaction 
  '', // стиль 
  '', // использование 
  '' // описание 
);


// Define the method as a PHP function
function getOrders($login, $password) {
  global $modx;
  $q = $modx->newQuery('msVendor');
  $q->where(array(
  	'login' => $login,
  	'password' => $password
  ));
  $total = $modx->getCount('msVendor', $q);
  if($total == 1){
    $q->prepare();
    $res = $modx->getObject('msVendor', $q);
    $idPartner = $res->get('id');
    $orders = getOrderPartner($idPartner);
    return $orders;
  }else{
    return '<?xml version="1.0" encoding="UTF-8"?><errors><error>Не верный логин или пароль</error></errors>';
  }
};

function getOrderPartner($idPartner) {
global $modx;
$response = $modx->runSnippet('GetOrderPartnerSoap', array(
  'tpl' => 'tpl.msGetOrderSoap',
  'tplWrapper' => 'tpl.msGetOrderSoapOutput',
  'vendorId' => $idPartner,
  ));
  return $response;
};

function changeStatus($login, $password, $idOrder){
global $modx;
if(authentication($login, $password)){
if($res = $modx->getObject('msOrder', $idOrder)){
$res->set('status', '5');
if ($res->save()){return true;}else{return false;};
}
else {return false;}
}else{
   return false;
}
};
function authentication($login, $password){
  global $modx;
  $q = $modx->newQuery('msVendor');
  $q->where(array(
  	'login' => $login,
  	'password' => $password
  ));
  $total = $modx->getCount('msVendor', $q);
  if($total == 1){
    return true;
  }else{
    return false;
  }
}
$server->service(file_get_contents("php://input"));
?>